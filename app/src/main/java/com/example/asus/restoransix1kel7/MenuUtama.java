package com.example.asus.restoransix1kel7;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MenuUtama extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_utama);
    }

    public void restaurant(View view){
        Intent intent = new Intent(MenuUtama.this, CariRestaurant.class);
        startActivity(intent);
    }

    public void pemesanan(View view){
        Intent intent = new Intent(MenuUtama.this, Pemesanan.class);
        startActivity(intent);
    }

    public void bayar(View view){
        Intent intent = new Intent(MenuUtama.this, bill.class);
        startActivity(intent);
    }

    public void adduser(View view){
        Intent intent = new Intent(MenuUtama.this, AddUser.class);
        startActivity(intent);
    }

    public void addrestaurant(View view){
        Intent intent = new Intent(MenuUtama.this, AddRestaurant.class);
        startActivity(intent);
    }

    public void addtransaction(View view){
        Intent intent = new Intent(MenuUtama.this, AddTransaction.class);
        startActivity(intent);
    }

}
