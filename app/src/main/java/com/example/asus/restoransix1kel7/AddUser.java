package com.example.asus.restoransix1kel7;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class AddUser extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user);
    }

    public void btnsimpan(View view){
        Toast.makeText(getApplicationContext(), "Data telah disimpan", Toast.LENGTH_SHORT).show();
    }

}
