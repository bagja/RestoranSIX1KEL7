package com.example.asus.restoransix1kel7;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Login extends AppCompatActivity {

    Button lgn, ccl;
    EditText usrnm, pass;
    String user, pss;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        lgn = (Button) findViewById(R.id.btnlogin2);
        ccl = (Button) findViewById(R.id.btncancel2);
        usrnm = (EditText) findViewById(R.id.etusername2);
        pass = (EditText) findViewById(R.id.etpassword2);

    }

    public void logins(View view){
        user = usrnm.getText().toString();
        pss = pass.getText().toString();
        if(user.equals("admin") && pss.equals("admin")){
            Intent intent = new Intent(Login.this, MenuUtama.class);
            startActivity(intent);
        }else{
            Toast.makeText(Login.this, "Username atau Password salah", Toast.LENGTH_LONG).show();
        }

    }

}
