package com.example.asus.restoransix1kel7;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class AddRestaurant extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_restaurant);
    }

    public void btnsimpan(View view){
        Toast.makeText(getApplicationContext(), "Data telah disimpan", Toast.LENGTH_SHORT).show();
    }

    public void btnupdate(View view){
        Toast.makeText(getApplicationContext(), "Data telah diperbaharui", Toast.LENGTH_SHORT).show();
    }

    public void btndelete(View view){
        Toast.makeText(getApplicationContext(), "Data telah terhapus", Toast.LENGTH_SHORT).show();
    }

}
