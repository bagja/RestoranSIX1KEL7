package com.example.asus.restoransix1kel7;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class CariRestaurant extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cari_restaurant);
    }

    public void rumahng(View view){
        Intent intent = new Intent(CariRestaurant.this, Pemesanan.class);
        startActivity(intent);
    }

    public void ngmafia(View view){
        Intent intent = new Intent(CariRestaurant.this, Pemesanan.class);
        startActivity(intent);
    }

    public void cari(View view){
        Toast.makeText(getApplicationContext(), "Sedang mencari restaurant", Toast.LENGTH_SHORT).show();
    }

}
